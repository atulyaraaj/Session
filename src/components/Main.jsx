import React from "react";
import "../Styles/Main.css";
const Main = () => {
  const data = [
    {
      i: "Assets/no-phone.svg",
      h: "No Phone Numbers",
      alt: "no-phone",
      p1: "Session accounts are compeletelt anonymous.",
      p2: "No phone number or email required.",
    },
    {
      i: "Assets/no-data.svg",
      h: "No Data Breaches",
      alt: "no-data",
      p1: "Session doesn't collect data,.",
      p2: "so there's nothing to leak.",
    },
    {
      i: "Assets/no-footprint.svg",
      h: "No Footprints",
      alt: "no-footprint",
      p1: "Send messages through own onion.",
      p2: "Routing network and leave no trace.",
    },
    {
      i: "Assets/open-source.svg",
      h: "Open Source",
      alt: "open source",
      p1: "Session's code nothing to hide.Anyone can",
      p2: "view, audit, and contribute.",
    },
    {
      i: "Assets/censorship-resistant.svg",
      h: "Censorship Resistant",
      alt: "censorship",
      p1: "With no central point of failure.",
      p2: "It's harder to shut session down.",
    },
  ];
  return (
    <>
      <div classNAme="main">
        <div className="section-1">
          <div className="heading">
            Send <br />
            Messages, <br />
            Not Metadata. <br />
            <button>DOWNLOAD</button>
          </div>
          <img src="/Assets/ui-showcase.webp" alt="ui-showcase" />
        </div>
        <div className="section-2">
          <p>what is Session ?</p>
          <p id="p2">
            Session is an <span> end-to-end </span> encrypted messenger that
            minimises <span> sensitive </span> metadata,{" "}
            <span> designed and built </span> for people who want{" "}
            <span> absolute </span> privacy and freedom from{" "}
            <span> any form of </span> surveillance.
          </p>
          <video controls>
            <source src="Assets/720p.mp4" type="video/mp4" alt="videoo" />
          </video>
        </div>
        <div className="section-3">
          <p>Benefits</p>
          <div>
            {data.map((prop, index) => (
              <div className="content" key={index}>
                <img src={prop.i} alt={prop.alt} />
                <h1>{prop.h}</h1>
                <p> {prop.p1}</p>
                <p> {prop.p2}</p>
              </div>
            ))}
          </div>
        </div>
        <div className="section-4">
          <div className="feature">
            <div>
              <h3 className="msg">Group chats</h3>
              <p className="para">
                Talk to your friends or talk to the world. You decide. Groups
                let you talk to up to 100 friends at once, with the same
                encrypted protections as one-on-one chats. Got a bigger crowd?
                Use a community to connect with as many people as you want.
                Voice messages Sometimes, a text just isn’t enough.
              </p>
            </div>
            <div>
              <h3 className="msg">Voice messages</h3>
              <p className="para">
                let you send something a little more personal, so nothing gets
                lost in translation.
              </p>
            </div>
            <div>
              <h3 className="msg">Attachments</h3>
              <p className="para">
                Don’t leak those docs. Send all your files, images, and
                attachments through a network that takes your privacy seriously.
              </p>
            </div>
          </div>
          <img src="Assets/mockup-desktop.webp" alt="mockup-desktop" />
        </div>
      </div>
    </>
  );
};
export default Main;
